#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

float calculateSD(float data[]);
float calculateMD(float data[]);
void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int *temp;
	int *humedad;
	float array_temp[12];
	float array_humedad[12];
	int posicion = 0;
	int intentos = 0;//cuando llegue a 3 termina el programa
	int muestra = 0; //cuando llegue a 12  se toma la estadistica y se reinicio
	int fd = -1;
	char te = 't';
	char hu = 'h';
	int max_t=0;
	int max_h=0;
	int min_t=999;
	int min_h=999;
	int baudrate = 9600;  // default
	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	temp = (int *)malloc(sizeof(int));
	humedad = (int *)malloc(sizeof(int));
	*temp = 0;
	*humedad = 0;
	sleep(3);
	write(fd,"1",1);
	printf("Adruino Funcionando...\n");
	serialport_flush(fd);
	while(muestra<13){
		sleep(5);
		write(fd,&te,1);
		usleep(5000);
		read(fd,temp,1);
		usleep(5000);
		write(fd,&hu,1);
		read(fd,humedad,1);
		if(muestra==0){

		}
		else{
			printf("Temperatura: %d. ",*temp);
			array_temp[muestra-1] = *temp;
			printf("Humedad: %d.\n",*humedad);
			array_humedad[muestra-1] = *humedad;
			
		
		if(*temp>max_t){
			max_t = *temp;
		}
		if(*humedad>max_h){
			max_h = *humedad;
		}
		if(*temp<min_t){
			min_t = *temp;
		}
		if(*humedad<min_h){
			min_h = *humedad;
		}
		}
		muestra++;
	}
	printf("\nMedia temperatura: %f.\n",calculateMD(array_temp));
	printf("Media humedad: %f.\n",calculateMD(array_humedad));
	printf("Desviacion estandar temperatura: %f.\n",calculateSD(array_temp));
	printf("Desviacion estandar humedad: %f.\n",calculateSD(array_humedad));
	printf("Minima temperatura: %d.\n",min_t);
	printf("Maxima temperatura: %d.\n",max_t);
	printf("Minima humedad: %d.\n",min_h);
	printf("Maxima humedad: %d.\n",max_h);
	write(fd,"0",1);
	close( fd );
	free(temp);
	free(humedad);
	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 12; ++i)
    {
        sum += data[i];
    }

    mean = sum/12;

    for(i = 0; i < 12; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 12);
}

float calculateMD(float data[]){
	float sum = 0.0, mean;

	int i;

	for(i = 0; i< 12; ++i)
	{
		sum += data[i];
	}

	mean = sum/12;

	return mean;
}
